import cv2
import os
import time
import uuid
import subprocess

image_path = "/Users/3016678/Documents/RealTimeObjectDetection/Tensorflow/workspace/images/collectedimages"
labels = ['hello', 'thanks', 'yes', 'no', 'iloveyou']
number_imgs = 15

for label in labels:
    subprocess.call(['mkdir', '/Users/3016678/Documents/RealTimeObjectDetection/Tensorflow/workspace/images/collectedimages//' + label])
    cap = cv2.VideoCapture(0)
    print('Image Collection for {}'.format(label))
    time.sleep(5)
    for imgnum in range(number_imgs):
        ret, frame = cap.read()
        imagename = os.path.join(image_path, label, label + '.'+'{}.jpg'.format(str(uuid.uuid1())))
        cv2.imwrite(imagename, frame)
        cv2.imshow('frame', frame)
        time.sleep(2)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    cap.release()

